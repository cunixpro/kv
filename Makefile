CFLAGS= -Wall -Werror -Wextra -pedantic

program: main.c hash.c
	cc ${CFLAGS} main.c hash.c -o kv

clean: 
	rm -f kv *.core
